package com.test;

import org.msf.gateway.weixin.clientsdk.WeixinApi;
import org.msf.gateway.weixin.in.handler.SimpleHandle;
import org.msf.services.TextResponse;
import org.msf.services.life.ColdJokeService;
import org.msf.services.life.MobileAreaService;
import org.msf.services.life.WeatherService;
import org.msf.services.music.douban.DoubanRadio;
import org.msf.services.music.sing5.Sing5Radio;

public class MyWxHandler extends SimpleHandle {

	@Override
	protected TextResponse[] getTextHandlers() {
		// 注册[天气,冷笑话,手机归属地]服务;
		return new TextResponse[] { new WeatherService(), new ColdJokeService(), new MobileAreaService(), new MyTextService() };
	}

	@Override
	protected void onMenuClick(String fromUser, String menuKey) {
		if (menuKey.indexOf("DOUBANFM_") != -1) {
			DoubanRadio.handle(menuKey.split("_")[1], fromUser);
			return;
		}
		if (menuKey.indexOf("5SINGFM_") != -1) {
			Sing5Radio.handle(menuKey.split("_")[1], fromUser);
			return;
		}
	}

	@Override
	protected void onSubscribe(String fromUser) {
		WeixinApi.sendCustomText(fromUser, "欢迎关注，回复以下关键字(文本或语音)即可查询到相应信息：冷笑话，xx(国内城市)天气 ，手机xxxxxxxxxxx(国内手机号)");
		// System.out.println(rs);
	}
}
