package com.test;

import java.io.IOException;

import org.msf.gateway.weixin.clientsdk.WeixinError;
import org.msf.gateway.weixin.clientsdk.http.WebUtils;
import org.msf.services.TextResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class MyTextService extends TextResponse {

	@Override
	public boolean matches(String text) {
		return "ip".equals(text);
	}

	@Override
	public String handler(String text, String fromuser) {
		StringBuilder result = null;
		try {
			JSONObject json = JSON.parseObject(WebUtils.doGet("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json"));
			result = new StringBuilder();
			result.append("【省份】").append(json.getString("province")).append("【城市】").append(json.getString("city"));
			return result.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		} finally {
			if (result != null)
				result.setLength(0);
		}
		return null;
	}

	public static void main(String[] args) {
		try {
			System.out.println(WebUtils.doGet("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (WeixinError e) {
			System.out.println(e.getMessage());
		}
	}
}
