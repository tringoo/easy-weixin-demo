/******************************************************************************
//canvaId: Canvas 2D context
//data: json 数据
//yInterval: Y轴一刻度的数值
*******************************************************************************/
function histogram(canvaId, data, yInterval) {
	var c = document.getElementById(canvaId);
    this.ctx = c.getContext("2d");
    //Json数据
    this.data = data;    
    //Y轴一刻度的数值
    this.per = screen.width/320;
    this.yInterval = yInterval;
    this.cfg = {
	    "canva_x":0  ,//画笔起点
	    "y_text_width":30,
	    "bar_width":40 * this.per,//柱状宽度
	    "bar_interval":10 * this.per,//柱状之间间隔
	    "font_size": 20 * this.per/2, //字体大小
	    "per":screen.width/320
    }
};
histogram.prototype.draw = function () {
	this.cfg.canva_x = this.cfg.font_size;
    //柱状图标题
     var title = this.data.title;
    //Y轴标题
     var verticaltitle = this.data.verticaltitle;
    //X轴标题
     var horizontaltitle = this.data.horizontaltitle;
    //颜色
    //var colors = ["#3366FF", "#FFCC00"];
    var dataarray = this.data.data;
    var dataCollection;
    var metaData;    
    var maxamount = 0;
    var categoryCount = dataarray.length;
    var dataCount = dataarray[0].datacollection.length;
    //找出最大的数值，以便绘制Y轴的刻度。
    for (var i = 0; i < dataarray.length; i++) {
        dataCollection = dataarray[i].datacollection;
        for (var j = 0; j < dataCollection.length; j++) {
            metaData = dataCollection[j];
            maxamount = (new Number(metaData.amount) > maxamount) ? metaData.amount : maxamount;
        }
    }
    //每个刻度高
    this.cfg.perYH = Math.ceil(50 * this.per);
    //动态设置 canvas 的尺寸
    this.ctx.canvas.height = Math.ceil(maxamount / this.yInterval)*this.cfg.perYH  + 120;    //120 is for the chart title.
    var vbar = new verticalbar(this.ctx, maxamount, this.yInterval,this.cfg);
    var hbar = new horizontalbar(this.ctx, categoryCount, dataCount,this.cfg);
    this.ctx.canvas.width = vbar.x_end + hbar.width;
    //绘制 x 轴和 y 轴
    vbar.draw();
    hbar.draw(vbar);
    //绘制标题
    this.ctx.font = "bold "+Math.ceil(this.cfg.font_size/1.2)+"px Arial";
    this.ctx.textAlign = "center";
    this.ctx.fillText(title, this.ctx.canvas.width/2,25);
    //绘制Y轴标题
    //this.ctx.font = this.cfg.font_size+"px Arial";
    //this.ctx.textAlign = "center";
    //this.ctx.fillText(verticaltitle, this.cfg.canva_x+this.cfg.y_text_width, 40);
    //绘制X轴标题
    //this.ctx.font = this.cfg.font_size+"px Arial";
    //this.ctx.fillText(horizontaltitle, this.cfg.canva_x +this.cfg.y_text_width+ hbar.width + 15, this.ctx.canvas.height - 46);
    //绘制柱
    this.ctx.lineWidth = this.cfg.bar_width;
    var x = vbar.x_end + this.cfg.bar_interval + Math.ceil(this.cfg.bar_width / 2);
    var y = this.ctx.canvas.clientHeight;
    for (var i = 0; i < dataarray.length; i++) {
        dataCollection = dataarray[i].datacollection;
        for (var j = 0; j < dataCollection.length; j++) {
            metaData = dataCollection[j];
            //用自定义的颜色
            this.ctx.beginPath();
			this.ctx.strokeStyle = metaData["barcolor"] || dataarray[i]["barcolor"] || "#000";
            this.ctx.fillStyle = metaData["textcolor"] || dataarray[i]["textcolor"] || "#000";
            this.ctx.moveTo(x, y - 50-1);
            this.ctx.lineTo(x, y - 50 - 1 - (metaData.amount / vbar.interval) * this.cfg.perYH +5);
            this.ctx.stroke();
            //绘制柱的数值
            this.ctx.textAlign = "start";
            //this.ctx.font = Math.ceil(this.cfg.font_size/1.5)+"px Arial";;
            //var n = metaData.amount;
            //var str = vbar.num2str(metaData.amount);
            //this.ctx.fillText(str, x - 12, y - 50 - 1 - (metaData.amount / vbar.interval) * this.cfg.perYH);
            //绘制柱的标题
            this.ctx.font = this.cfg.font_size+"px Arial";            
            this.ctx.fillText(metaData.title, x-12 , y - 30);
            x += (categoryCount * this.cfg.bar_interval + this.cfg.bar_width);
        }
        x = this.cfg.canva_x + this.cfg.bar_interval * (i + 1);       
    }
    //绘制右上角的标识器
    //this.ctx.lineWidth = 15;    
    //for (var i = 0; i < dataarray.length; i++) {
    //    this.ctx.beginPath();
    //    this.ctx.strokeStyle = colors[i];
    //    this.ctx.moveTo(this.cfg.canva_x + hbar.width + 30, 50+i*17);
    //    this.ctx.lineTo(this.cfg.canva_x + hbar.width + 30 + 10, 50 + i * 17);
    //    this.ctx.font = this.cfg.font_size+"px Arial";
    //    this.ctx.textAlign = "left";
    //    this.ctx.fillText(dataarray[i].category, this.cfg.canva_x + hbar.width - 10, 50 + i * 17+4);
    //    this.ctx.stroke();
    //}
}
/******************************************************************************
//X轴
//ctx: Canvas 2D context
//categoryCount: 数据的分类
//dataCount: 每一分类的数据样本数
*******************************************************************************/
function horizontalbar(ctx, categoryCount, dataCount,cfg) {
    this.ctx = ctx;
    this.categoryCount = categoryCount;
    this.dataCount = dataCount;
    this.cfg = cfg;
    //计算X轴的长度
    this.width = this.categoryCount * (this.dataCount+1) * this.cfg.bar_interval + this.dataCount*this.cfg.bar_width;
};
horizontalbar.prototype.draw = function (vbar) {
    //绘制X轴
    this.ctx.beginPath();
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = "#999999";
    this.ctx.moveTo(vbar.x_end, this.ctx.canvas.clientHeight - 50);
    this.ctx.lineTo(vbar.x_end+this.width, this.ctx.canvas.clientHeight - 50);
    this.ctx.stroke();
};
/******************************************************************************
//Y轴
//ctx: Canvas 2D context
//maxAmount: 数据的最大值
//interval: 每一刻度代表的数值
*******************************************************************************/
function verticalbar(ctx, maxAmount, interval,cfg) {
    this.ctx = ctx;
    this.maxAmount = maxAmount;
    this.interval = interval;
    this.cfg = cfg;
    this.x_end = this.cfg.canva_x + this.cfg.y_text_width +5;
};
verticalbar.prototype.draw = function (x) {
    //计算需要绘制几个刻度
    var segmentcount = Math.ceil(this.maxAmount / this.interval);
    //Y轴的高度
    var height = segmentcount * this.cfg.perYH;
    //绘制Y轴
    this.ctx.beginPath();
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = "#999999";
    this.ctx.moveTo(this.x_end, this.ctx.canvas.clientHeight  - 50);
    this.ctx.lineTo(this.x_end, this.ctx.canvas.clientHeight - 50 - height - 20);
    this.ctx.stroke();
    //绘制刻度数
    this.ctx.font = this.cfg.font_size+"px Arial";
    this.ctx.textAlign = "end";
    //this.ctx.fillText("0", this.cfg.canva_x - 40, this.ctx.canvas.clientHeight - 50);//坐标0
    for (var j = 1; j < segmentcount + 1; j++) {
	    //this.ctx.fillText(this.interval * j, this.cfg.canva_x - 40, this.ctx.canvas.clientHeight - 20 - (j - 1) * 50 - 70);
	    var str = this.num2str(this.interval * j);
        this.ctx.fillText(str, this.cfg.canva_x+28, this.ctx.canvas.clientHeight - 50 - (j - 1) * this.cfg.perYH - this.cfg.perYH);
    }
};
verticalbar.prototype.num2str = function(n){
	return n==10?"偏小":(n==20?"正常":"偏大");
}

 